

/**
 * Functional Class Pattern:
 * Object decorator
 * @param obj
 * @param loc
 * @returns {*}
 */
var carlike = function (obj, loc) {
    obj.loc = loc;
    obj.move = function () {
        obj.loc++;
    };
    return obj;
};
var car1 = carlike({}, 4);

/**
 * Functional Class Pattern
 * @param loc
 * @returns {{loc: *}}
 * @constructor
 */
var Car = function (loc) {
    var obj = {loc: loc};
    obj.move = function () {
        obj.loc++;
    };
    return obj;
};

var car2 = Car(5);

/**
 * Functional SubClassing
 * @param loc
 * @returns {{loc: *}}
 * @constructor
 */
var Van = function (loc) {
    var obj = Auto(loc);
    obj.grab = function () {};
    return obj;
};

/**
 * Prototypal Pattern
 * @param loc
 * @returns {CarProto}
 * @constructor
 */
var CarProto = function (loc) {
    var obj = Object.create(CarProto.prototype);
    obj.loc = loc;
    return obj;
};

CarProto.prototype = {
    move : function () {
        this.loc++;
    }
};

/**
 * PseudoClassical Pattern
 * @param loc
 * @constructor
 */
var CarPseudo = function (loc) {
    this.loc = loc;
};

CarPseudo.prototype = {
    move : function () {
        this.loc++;
    }
};

var VanPseudo = function (loc) {
    CarPseudo.call(this, loc);
};
VanPseudo.prototype = Object.create(CarPseudo.prototype);
VanPseudo.prototype.grab = function () {};
VanPseudo.prototype.constructor = VanPseudo;

var car3 = new CarPseudo(5);
var van3 = new VanPseudo(5);

var switchProto = {
        isOn: function isOn() {
            return this.state;
        },

        toggle: function toggle() {
            this.state = !this.state;
            return this;
        },

        meta: {
            name: 'Light switch'
        },

        state: false
    },
    switch1 = Object.create(switchProto),
    switch2 = Object.create(switchProto);

function Car(color, direction, mph) {
    this.color = color || 'pink';
    this.direction = direction || 0; // 0 = Straight ahead
    this.mph = mph || 0;

    this.gas = function gas(amount) {
        amount = amount || 10;
        this.mph += amount;
        return this;
    };

    this.brake = function brake(amount) {
        amount = amount || 10;
        this.mph = ((this.mph - amount) < 0)? 0
            : this.mph - amount;
        return this;
    };
}

var betterCar = function (color, direction, mph) {
    var isParkingBrakeOn = false;

    return {
        color: color || 'pink',
        direction: direction || 0,
        mph: mph || 0,
        gas: function gas(amount) {
            amount = amount || 10;
            this.mph += amount;
            return this;
        },

        brake: function brake(amount) {
            amount = amount || 10;
            this.mph = ((this.mph - amount) < 0) ? 0
                : this.mph - amount;
            return this;
        },

        toggleParkingBrake: function toggleParkingBrake() {
            isParkingBrakeOn = !isParkingBrakeOn;
            return this;
        },

        isParked: function isParked() {
            return isParkingBrakeOn;
        }
    };
};

var testCar = betterCar('red', 'north', 35);

var CarPrototype = {
    gas: function gas(amount) {
        amount = amount || 10;
        this.mph += amount;
        return this;
    },
    brake: function brake(amount) {
        amount = amount || 10;
        this.mph = ((this.mph - amount) < 0)? 0
            : this.mph - amount;
        return this;
    },

    color: 'pink',
    direction: 0,
    mph: 0
};

var car = function car(options) {
    var isParkingBrakeOn = false;

    options = options || {};

    options.toggleParkingBrake = function toggleParkingBrake() {
        isParkingBrakeOn = !isParkingBrakeOn;
        return this;
    };
    options.isParked = function isParked() {
        return isParkingBrakeOn;
    };
    return $.extend(Object.create(CarPrototype), options);
};

var testDrive = car({color: 'red'});

debugger;




