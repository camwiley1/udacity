

/**
 *  Method 1 (No encapsulation)
 *  a. Delegate Prototype
 *  b. Instance State
 */
var carProto = {
    gas  : function gas(amount) {
        amount = amount || 10;
        this.mph += amount;
        return this;
    },
    brake: function brake(amount) {
        amount = amount || 10;
        this.mph = ((this.mph - amount) < 0) ? 0
            : this.mph - amount;
        return this;
    },

    color    : 'pink',
    direction: 0,
    mph      : 0
};

var car = function car(options) {
    return $.extend(Object.create(carProto), options);
};

var toyota = car({color: 'red'});

var auto = function (options) {
    return $.extend(Object.create(auto.prototype), options);
};

auto.prototype = {
    move: function () {
        this.loc++;
        return this;
    }
};

/**
 *  Method 2 (Full coverage)
 *  a. Delegate Prototype
 *  b. Instance State
 *  c. Encapsulation
 */
var fancyCarProto = function () {
    var isParkingBrakeOn = false;

    return {
        gas  : function gas(amount) {
            amount = amount || 10;
            this.mph += amount;
            return this;
        },
        brake: function brake(amount) {
            amount = amount || 10;
            this.mph = ((this.mph - amount) < 0) ? 0
                : this.mph - amount;
            return this;
        },

        color    : 'pink',
        direction: 0,
        mph      : 0,
        toggleParkingBrake: function toggleParkingBrake() {
            isParkingBrakeOn = !isParkingBrakeOn;
            return this;
        },
        isParked : function isParked() {
            return isParkingBrakeOn;
        }
    };
}();

// Delegate Prototype & Instance State
var fancyCar = function (options) {
    return $.extend(Object.create(fancyCarProto), options);
};

var myCar = fancyCar({color: 'red', name: 'mine'});
debugger;



