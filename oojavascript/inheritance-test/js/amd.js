/**
 * Created by wileyc1 on 3/2/15.
 */

var app = {};

(function (exports) {

    (function (exports) {
        var api = {
            moduleExists: function test() {
                return true;
            }
        };
        $.extend(exports, api);

        if (typeof define === 'function') {
            define([], function () {
                return api;
            });
        }

    }((typeof exports === 'undefined') ?
      window : exports));

}(app));

test('Pass app as exports.', function () {
    ok(app.moduleExists(),
        'The module exists.');
});
