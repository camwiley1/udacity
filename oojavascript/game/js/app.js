var colMax = 4,
    rowMax = 5;

var Character = function () {
    this.height = 83;
    this.width  = 101;
    this.col = 0;
    this.row = 0;
    this.sprite = '';
};

// Draw the enemy on the screen, required method for game
Character.prototype.render = function() {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};

Character.prototype.handleInput = function (direction) {
    if (direction === 'left') {
        this.moveLeft();
    } else if (direction === 'right') {
        this.moveRight();
    } else if (direction === 'up') {
        this.moveUp();
    } else if (direction === 'down') {
        this.moveDown();
    }

    this.update();
};

Character.prototype.moveUp = function () {
    this.row--;
};

Character.prototype.moveDown = function () {
    this.row++;
};

Character.prototype.moveLeft = function () {
    this.col--;
};

Character.prototype.moveRight = function () {
    this.col++;
};

// Enemies our player must avoid
var Enemy = function() {

    Character.call(this);

    // Variables applied to each of our instances go here,
    // we've provided one for you to get started

    // The image/sprite for our enemies, this uses
    // a helper we've provided to easily load images
    this.sprite = 'images/enemy-bug.png';
    this.col = 0;
    this.row = 1;
    this.x = 0;
    this.y = 0;
};

Enemy.prototype = Object.create(Character.prototype);
Enemy.prototype.constructor = Enemy;

// Update the enemy's position, required method for game
// Parameter: dt, a time delta between ticks
Enemy.prototype.update = function(dt) {
    //You should multiply any movement by the dt parameter
    //which will ensure the game runs at the same speed for
    //all computers.
    var maxWidth = (colMax + 1)  * this.width;

    this.x = this.x + (100 * dt);
    console.log(this.x);
    if (this.x > maxWidth) {
        this.x = -this.width;
    }

    this.y = this.row * this.height;
};


// Now write your own player class
// This class requires an update(), render() and
// a handleInput() method.

var Player = function () {
    Character.call(this);
    this.sprite = 'images/char-boy.png';
    this.col = 2;
    this.row = 5;
};

Player.prototype = Object.create(Character.prototype);
Player.prototype.constructor = Player;
Player.prototype.update = function () {
    this.col = Math.max(this.col, 0);
    this.col = Math.min(this.col, colMax);

    this.x = this.col * this.width;

    this.row = Math.max(this.row, 0);
    this.row = Math.min(this.row, rowMax);

    this.y = this.row * this.height;
};

//Player.prototype.update = function () {
//    this.col = Math.max(this.col, 0);
//    this.col = Math.min(this.col, 4);
//
//    this.x = this.col * this.width;
//
//    this.row = Math.max(this.row, 0);
//    this.row = Math.min(this.row, 5);
//
//    this.y = this.row * this.height;
//};

// Now instantiate your objects.
// Place all enemy objects in an array called allEnemies
// Place the player object in a variable called player
player = new Player();

allEnemies = [];
allEnemies.push(new Enemy());

// This listens for key presses and sends the keys to your
// Player.handleInput() method. You don't need to modify this.
document.addEventListener('keyup', function(e) {
    var allowedKeys = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down'
    };

    player.handleInput(allowedKeys[e.keyCode]);
});
